"""Creates main data file."""

import datetime
import json
import pandas as pd

# read data from UZIS
url = "https://onemocneni-aktualne.mzcr.cz/api/account/verejne-distribuovana-data/file/dip%252Fweb_orp.csv"
df = pd.read_csv(url, delimiter=";")

# correct ORP codes from UZIS to CZSO
df.loc[df['orp_kod'] == 0, 'orp_kod'] = 1000 # Praha
df.loc[(df['orp_kod'] > 2000) & (df['orp_kod'] < 3000), 'orp_kod'] = df.loc[(df['orp_kod'] > 2000) & (df['orp_kod'] < 3000)]['orp_kod'].add(100)    # Stredni Cechy
df.loc[(df['orp_kod'] > 6000) & (df['orp_kod'] < 7000), 'orp_kod'] = df.loc[(df['orp_kod'] > 6000) & (df['orp_kod'] < 7000)]['orp_kod'].add(-200)    # Jizni Morava, Vysocina
df.loc[(df['orp_kod'] > 8000) & (df['orp_kod'] < 9000), 'orp_kod'] = df.loc[(df['orp_kod'] > 8000) & (df['orp_kod'] < 9000)]['orp_kod'].add(100)    # Moravskoslezsky

# set dates
today = datetime.date.today()
last_ok_day = today + datetime.timedelta(days=-1)
last_ok_date = last_ok_day.isoformat()

# filter last ok date
selected = df[df['datum'] == last_ok_date]

# load orps and municipalities
orp = pd.read_csv("orp_population.csv")
municipalities = pd.read_csv("municipality_population.csv")

# merging, calculating
orps = orp.merge(selected, left_on='code', right_on='orp_kod')
orps['prevalence_rate'] = orps['prevalence'] / orps['population']

orpdata = orps.filter(['code', 'name', 'prevalence_rate'])
orpdata = orpdata.rename(columns={'code': 'c', 'name': 'n', 'prevalence_rate': 'r'})
orpdata = orpdata.sort_values('r')

municipalities = municipalities.merge(orps, left_on='orp_code', right_on='code')
municipalities = municipalities.sort_values('population_x', ascending=False)

data = municipalities.filter(['code_x', 'name_x', 'orp_code', 'orp_name', 'prevalence_rate'])
data = data.rename(columns={'code_x': 'c', 'name_x': 'n', 'orp_code': 'oc', 'orp_name': 'on', 'prevalence_rate': 'r'})

# save current rates by muni and orp
data.to_csv('data.csv', index=False)
dobj = data.to_dict('records')
with open("data.json", "w") as fout:
    json.dump(dobj, fout, ensure_ascii=False)

orpdata.to_csv('orpdata.csv', index=False)
orpdataobj = orpdata.to_dict('records')
with open("orpdata.json", "w") as fout:
    json.dump(orpdataobj, fout, ensure_ascii=False)